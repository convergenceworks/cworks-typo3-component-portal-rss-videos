<?php
class ComponentController_rss_videos {

    function render(){

        $data = array();
        $data['videos'] = $this->providerData['externalVideos'];
        $data['title'] =  $this->helper->getFlexformFieldDataText($this->flexform,'sRSSVIDEOS','title');
        $data['link'] = $this->helper->getFlexformFieldDataLink($this->flexform,'sRSSVIDEOS','link');
        $data['description'] = $this->helper->getFlexformFieldDataText($this->flexform,'sRSSVIDEOS','description');
        $data['linkrss'] = $this->helper->getTypolink($this->helper->getParentPageData('tx_cwnews_default_rss'));

        return array("data" => $data);

    }
}
?>
